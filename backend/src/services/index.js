const student = require('./student/student.service.js');
module.exports = function () {
  const app = this;
  app.configure(student);
};
