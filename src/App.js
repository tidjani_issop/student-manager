import React, { Component } from 'react';
import { NavLink, Route } from 'react-router-dom';
import { Container } from 'semantic-ui-react';
import StudentListPage from './pages/student-list-page';
import StudentFormPage from './pages/student-form-page';

class App extends Component {
  render() {
    return (
      <Container>
        <div className="ui two item menu">
          <NavLink className="item" activeClassName="active" exact to="/">
            Students List
          </NavLink>
          <NavLink className="item" activeClassName="active" exact to="/students/new">
            Add Student
          </NavLink>
        </div>
        <Route exact path="/" component={StudentListPage}/>
        <Route path="/students/new" component={StudentFormPage}/>
        <Route path="/students/edit/:_id" component={StudentFormPage}/>
      </Container>
    );
  }
}

export default App;
