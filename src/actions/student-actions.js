import { client } from './';

const url = '/students';

export function fetchStudents(){
  return dispatch => {
    dispatch({
      type: 'FETCH_STUDENTS',
      payload: client.get(url)
    })
  }
}

export function newStudent() {
  return dispatch => {
    dispatch({
      type: 'NEW_STUDENT'
    })
  }
}

export function saveStudent(student) {
  return dispatch => {
    return dispatch({
      type: 'SAVE_STUDENT',
      payload: client.post(url, student)
    })
  }
}

export function fetchStudent(_id) {
  return dispatch => {
    return dispatch({
      type: 'FETCH_STUDENT',
      payload: client.get(`${url}/${_id}`)
    })
  }
}

export function updateStudent(student) {
  return dispatch => {
    return dispatch({
      type: 'UPDATE_STUDENT',
      payload: client.put(`${url}/${student._id}`, student)
    })
  }
}

export function deleteStudent(_id) {
  return dispatch => {
    return dispatch({
      type: 'DELETE_STUDENT',
      payload: client.delete(`${url}/${_id}`)
    })
  }
}
