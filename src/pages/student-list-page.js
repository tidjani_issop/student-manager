import React, { Component} from 'react';
import { connect } from 'react-redux';
import StudentList from '../components/student-list';
import { fetchStudents, deleteStudent } from '../actions/student-actions';

class StudentListPage extends Component {

  componentDidMount() {
    this.props.fetchStudents();
  }

  render() {
    return (
      <div>
        <h1>List of Students</h1>
        <StudentList students={this.props.students} deleteStudent={this.props.deleteStudent}/>
      </div>
    )
  }
}

// Make students  array available in props
function mapStateToProps(state) {
  return {
    students : state.studentStore.students
  }
}

export default connect(mapStateToProps, {fetchStudents, deleteStudent})(StudentListPage);
