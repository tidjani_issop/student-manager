import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import StudentReducer from './student-reducer';

const reducers = {
  studentStore: StudentReducer,
  form: formReducer
}

const rootReducer = combineReducers(reducers);

export default rootReducer;
