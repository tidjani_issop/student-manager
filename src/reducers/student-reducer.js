const defaultState = {
  students: [],
  student: { name: {} },
  loading: false,
  errors: {}
}

export default (state=defaultState, action={}) => {
  switch (action.type) {
    case "FETCH_STUDENTS_FULFILLED": {
      return {
        ...state,
        students: action.payload.data.data || action.payload.data // in case pagination is disabled
      }
    }

    case 'NEW_STUDENT': {
      return {
        ...state,
        student: { name: {} }
      }
    }

    case 'SAVE_STUDENT_PENDING': {
      return {
        ...state,
        loading: true
      }
    }

    case 'SAVE_STUDENT_FULFILLED': {
      return {
        ...state,
        students: [...state.students, action.payload.data],
        errors: {},
        loading: false
      }
    }

    case 'SAVE_STUDENT_REJECTED': {
      const data = action.payload.response.data;
      const { "name.first":first, "name.last":last, phone, email } = data.errors;
      const errors = { global: data.message, name: { first,last }, phone, email };

      return {
        ...state,
        errors: errors,
        loading: false
      }
    }

    case 'FETCH_STUDENT_PENDING': {
      return {
        ...state,
        loading: true,
        student: { name: {} }
      }
    }

    case 'FETCH_STUDENT_FULFILLED': {
      return {
        ...state,
        student: action.payload.data,
        errors: {},
        loading: false
      }
    }

    case 'UPDATE_STUDENT_PENDING': {
      return {
        ...state,
        loading: true
      }
    }

    case 'UPDATE_STUDENT_FULFILLED': {
      const student = action.payload.data;
      return {
        ...state,
        students: state.students.map(item => item._id === student._id ? student : item),
        errors: {},
        loading: false
      }
    }

    case 'UPDATE_STUDENT_REJECTED': {
      const data = action.payload.response.data;
      const { "name.first":first, "name.last":last, phone, email } = data.errors;
      const errors = { global: data.message, name: { first,last }, phone, email };
      return {
        ...state,
        errors: errors,
        loading: false
      }
    }

    case 'DELETE_STUDENT_FULFILLED': {
      const _id = action.payload.data._id;
      return {
        ...state,
        students: state.students.filter(item => item._id !== _id)
      }
    }

    default:
      return state;
  }
}
