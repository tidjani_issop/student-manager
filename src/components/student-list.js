import React from 'react';
import { Card } from 'semantic-ui-react';
import StudentCard from './student-card';

export default function StudentList({students, deleteStudent}){

  const cards = () => {
    return students.map(student => {
      return (
        <StudentCard
        key={student._id}
        student={student}
        deleteStudent={deleteStudent} />
      )
    })
  }

  return (
    <Card.Group>
      { cards() }
    </Card.Group>
  )
}
