This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Install dependencies

In the project directory && in the backend directory, you can run:

### `npm install`

## Run the app

In the backend directory before, run:

### `npm start`

Then you can run this again at root of the project.  
Runs the app in the development mode.  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.  

The page will reload if you make edits.  
You will also see any lint errors in the console.  

### `npm test`

Launches the test runner in the interactive watch mode.
